// http://learn.jquery.com/jquery-ui/widget-factory/why-use-the-widget-factory/
// http://wiki.jqueryui.com/w/page/12138135/Widget%20factory

$.Velocity.RegisterUI("animation.slideToRight", {
     defaultDuration: 900,
     calls: [
         [ { translateX: [0, -200], opacity: [1, 0] }, 0.50, { easing: 'easeIn' }  ]
     ],
     reset: { translateX: 0 }
});

$.Velocity.RegisterUI("animation.slideToLeft", {
     defaultDuration: 900,
     calls: [
         [ { translateX: [0, -200] }, 0.50 ]
     ]
});

(function ($, undefined) {

    'use strict';

    $.widget('mg.mmenu', {
        /*
            DATA example:
            [
                {"title": "Administrativo", "id": "administrativo", "href": ""},
                {"title": "Tela", "id": "tela", "href": "", "icon": "fa fa-folder-o", "inline-children": false, "children":
                    [
                        {"title": "Tela 1", "id": "tela-1", "href": ""},
                        {"title": "Tela 2", "id": "tela-2", "href": ""},
                        {"title": "Tela 4", "id": "tela-4", icon: "fa fa-icon-star", "href":""}
                    ]
                },
                {"title": "Automotivo", "id": "automotivo", "href": ""},
                {"title": "Configuração", "id": "configuracao", "href": ""},
                {"title": "Segurança", "id": "seguranca", "count": 10},                
                {"title": "Favoritos", "id": "favoritos", "row-before": true, "row-after": true, "href": ""},
                {"title": "Sair", "id": "sair"}
            ]

            Generated HTML example: 

            <nav class="menu">
                <ul class="menu-section" role="menu">
                    <li role="menuitem"><a href="#menu-2">Home</a></li>
                    <li><a href="#"><i></i> Administrativo</a></li>
                    <li><a href="#">Sair</a></li>
                </ul>
                <ul class="menu-section" id="menu-2">
                    <li><a href="#">Configuração</a></li>
                    <li><span>Sistema</span>
                        <ul>
                            <li><a href="#">Tela</a></li>
                            <li><a href="#">Módulo</a></li>
                            <li><a href="#">Plano comercial</a></li>
                            <li><a href="#menu-3">Permissão</a></li>
                        </ul>
                    </li>  
                </ul>
                <ul class="menu-section" id="menu-3">
                    <li><a href="#">Teste</a></li>
                    <li><a href="#">Permissões</a></li>
                </ul>
            </nav>
        */

        version: '1.0.0',
        // Options to be used as defaults
        options: {           
            mode: 'top',
            columnWidth: 167,
            columnTransition: 'slideRight',
            cssClass: 'mmenu',
            data: null
        },

        _arrSection: [],
        _indexSection: 0,

        //Setup widget (eg. element creation, apply theming
        // , bind events etc.)
        _create: function (event) {

            // create markup
            // create by existing markup or data
            this.menu = $('<nav class="' + this.options.cssClass + ' ' + this.options.mode + '"></nav>').appendTo(this.element);

            this._createMenuFromData();

            this.sections = this.menu.find('.menu-section');
            this._setupEvents();

            return this;
        },
        _createDataFromHTML: function() {

        },
        _getChildrenToSectionHTML: function(d, x) {
            var self = this, f = null, id = null, childIndex = 0, hasChildren = false;

            this._arrSection[x] += x === 0 ? '<ul class="menu-section" role="menu"><li><a href="#">Home</a></li>' : '';
            //debugger;
            $(d).each(function(){
                f = this;
                hasChildren = f.hasOwnProperty('children') && f.children.length > 0;

                // generate id or get one

                // mount li
                self._arrSection[x] +=
                    '<li role="menuitem"' + (hasChildren ? 'class="menu-arrow"' : '') + '>' +
                    '<a id="'+ f.id +'" ' + (hasChildren ? ' data-child="menu-section-' + f.id + '"' : 'href="' + f.href + '"') + '>' +
                        (f.icon ? '<i class="' + f.icon + '"></i>' : '') + f.title +
                    '</a>' +
                    '</li>';

                //self._arrSection[x] += sprintf('<li role="menuitem"><a id="%s" href="%s">%s %s</a></li>', f.id, f.hasOwnProperty('href') ? f.href : '#', function(){ return f.icon ? '<i class="' + f.icon + '"></i>' : ''; }, f.title);

                if (hasChildren) {
                    childIndex = x + 1 + self._indexSection;
                    self._arrSection[childIndex] += sprintf('<ul class="menu-section menu-section-%s" id="menu-section-%s" role="menu">', f.id, f.id);
                    self._arrSection[childIndex] += sprintf('<li><a class="mmenu-previous-section" href="#">%s</a></li>', f.hasOwnProperty('childrenTitle') ? f.childrenTitle : f.title);
                    self._getChildrenToSectionHTML(f.children, childIndex);
                    //console.log(self._arrSection);
                    self._indexSection = self._indexSection + 1;
                }
            });
            this._arrSection[x] += '</ul>';
        },
        _createMenuFromData: function() {
            var d = this.options.data,
                html = "";

            console.log(d);

            //debugger;
            //html += '<ul class="menu-section">';
            html = this._getChildrenToSectionHTML(d, 0);



            console.log(this._arrSection);

            this.menu.append(this._arrSection.join('').replace(/undefined/gi,''));
            //html += '</ul>';
        },
        _createMenu: function(field) {

        },
        _setupEvents: function () {
            this._on(this.menu, { 'click a': '_itemClick' });
            this._on($('.mmenu-toggle'), { 'click': '_toggleMenu' });
//            this._on(this.flipper, { click: "_flipperClick" });
//            this._on(this.element, { change: "refresh" })
//            this._on(this.handler, { keydown: "_tabKeydown", focus: "_handlerFocus", blur: "_handlerBlur", click: "_handlerClick" });
        },
        _toggleMenu: function(e) {
            e.preventDefault();
            //alert('toggle');
            if (this.menu.find('.menu-section').filter(':visible').length > 0)
                this.menu.show();
            else
                this.menu.children('.menu-section:first-child').velocity('transition.slideDownIn', { display: 'inline-block', duration: 200 });
        },
        _itemClick: function (e) {
            // prevent label wrapper conflict            
            e.preventDefault();            
            // open section

            var el = $(e.target),
            href = el.attr('href'),
            thisLi = el.closest('li'),
            thisSection = el.closest('.menu-section'),
            thisSectionAllLis = thisSection.find('li'),
            nextSection = thisSection.next(),
            allSections = this.menu.find('.menu-section'),
            allNextSections = thisSection.nextAll(),
            thisAndAllNextSections = allSections.not(thisSection.prevAll()),
            previousSection = thisSection.prev(),
            openingSection = $(href);

            if (el.data('child')) {
                var thisSectionLis = thisSection.find('li'),
                    allNextVisibleSections = allNextSections.filter(":visible");

                if (thisSectionLis.filter('.selected').length > 0 && allNextVisibleSections.length > 0) {
                    // check if were not any selected li's on the section, close all next sections if true
                    // remove all next selected classes if true
                    this.menu.find('li').removeClass('selected');
                    allNextVisibleSections.velocity('transition.slideLeftBigOut', {
                        stagger: 50,
                        backwards: true,
                        duration: 200,
                        complete: function() {
                            $('#' + el.data('child')).velocity('transition.slideLeftBigIn', {
                                display: 'inline-block',
                                zIndex: '=-1',
                                duration: 200
                            });
                        }
                    });
                } else {
                    $('#' + el.data('child')).velocity('transition.slideLeftBigIn', {
                        display: 'inline-block',
                        zIndex: '=-1',
                        duration: 200
                    });
                }
                thisLi.addClass('selected');
            } else if (el.hasClass('mmenu-previous-section')) {
                console.log(thisAndAllNextSections.filter(":visible"));
                thisAndAllNextSections.filter(":visible").velocity('transition.slideLeftBigOut', {
                    stagger: 250,
                    backwards: true,
                    duration: 200
                });
                console.log(previousSection);
                previousSection.find('li').removeClass('selected');
            }
        },
        _init: function () {
            //console.log('init');
            this.refresh();
        },
        refresh: function () {
            // extends data tags (optional)
//            $.extend(true, this.options, this.element.data());
//
//            var self = this;
//
//            // Set options
//            self.flipper.width(self.options.width);
//            self.flipper.addClass(self.options.css);
//            self.flipperLeft.text(self.options.offText);
//            self.flipperRight.text(self.options.onText);
//
//            if (self.element.prop('checked'))
//                self.turnOn();
//            else
//                self.turnOff();
//
//            //console.log('refresh', self.element);
//
//            if (self.element.prop('disabled') || !self.options.enabled)
//                self.disable();
//            else
//                self.enable();



        },
        _destroy: function () {
//            this._off(this.flipper, { click: "_flipperClick" });
//            this._off(this.element, { change: "refresh" })
//            this._off(this.handler, { keydown: "_tabKeydown", focus: "_handlerFocus", blur: "_handlerBlur", click: "_handlerClick" });
//            this.flipper.remove();
//            this.element.show();
        },
        _handlerClick: function (event) {
            event.preventDefault();
        },
        _handlerFocus: function () {
//            this.flipper.addClass('focus');
        },
        _handlerBlur: function () {
//            this.flipper.removeClass('focus');
        },
        _tabKeydown: function (event) {
//            var self = this;
//            switch (event.keyCode) {
//                case $.ui.keyCode.SPACE:
//                    event.preventDefault();
//                    self.toggle();
//                    return;
//                case $.ui.keyCode.UP:
//                    event.preventDefault();
//                    self.turnOn();
//                    return;
//                case $.ui.keyCode.DOWN:
//                    event.preventDefault();
//                    self.turnOff();
//                    return;
//                case $.ui.keyCode.RIGHT:
//                    event.preventDefault();
//                    self.turnOn();
//                    return;
//                case $.ui.keyCode.LEFT:
//                    event.preventDefault();
//                    self.turnOff();
//                    return;
//                case $.ui.keyCode.ENTER:
//                    event.preventDefault();
//                    self.toggle();
//                    return;
//                default:
//                    return;
//            }
        },
        turnOn: function () {           

//            this.element.prop('checked', true);
//            this.flipper.addClass('on').removeClass('off');
//
//            this._trigger("afterTurnOn", null, [this.element]);
        },
        turnOff: function () {           

//            this.element.prop('checked', false);
//            this.flipper.addClass('off').removeClass('on');
//
//            this._trigger("afterTurnOff", null, [this.element]);
        },
        enable: function () {           

//            this.element.prop('disabled', false);
//            this.flipper.removeClass('disabled');
//            this.handler.prop('tabindex', '1');
//
//            this._trigger("afterEnable", null, [this.element]);
        },
        disable: function () {           

//            this.element.prop('disabled', true);
//            this.flipper.addClass('disabled');
//            this.handler.prop('tabindex', '-1');
//
//            this._trigger("afterDisable", null, [this.element]);
        },
        toggle: function () {

//            if (this.element.prop('checked'))
//                this.turnOff();
//            else
//                this.turnOn();
        }
    });

    // No Conflict
    var old = $.fn.mmenu;
    $.fn.mmenu.noConflict = function () {
        $.fn.mmenu = old;
        return this;
    }

}(jQuery));







// var width = 200,
//     allSections = $('.menu-section');


// // Menu effects






// $.Velocity.RegisterUI('slideLeft', {
//     defaultDuration: 500,
//     calls: [
//         [ { translateX: [-100, 0], opacity: [0, 1], zIndex: '+=1'}, { display: 'none' }, 50 ]
//     ],
//     reset: { translateX: 0, zIndex: 99 }
// });


// $(document).click(function (e) {
//     var container = $('.menu');
//     console.log($('.toggle-menu').has(e.target).length);
//     if (!container.is(e.target) && container.has(e.target).length === 0 && !$('.toggle-menu').has(e.target).length > 0) {
//         //console.log('click.out');
//         //allSections.velocity('transition.slideLeftOut', { stagger: 150 });
//         /*allSections
//             .velocity('stop', true)
//             .velocity('transition.slideLeftOut', { stagger: 150 });
//         */
//     }
// });

// // menu

// $('.toggle-menu').click(function(){
//     $('.menu .menu-section:first-child').velocity('animation.slideToRight', { display: 'inline-block' }, 10);
//     //$('.menu .menu-section:first-child').velocity('transition.slideDownIn', { display: 'inline-block' }, 10);
//     //$('.menu .menu-section:first-child li').velocity('transition.fadeIn', { 'opacity': 0 }, { stagger: 150 });
// });

// $('.menu').on('click', 'a', function(e) {
//     e.preventDefault();
//     var el = $(this),
//         href = el.attr('href'),
//         thisLi = el.closest('li'),
//         thisSection = el.closest('.menu-section'),
//         allLiSection = thisSection.find('li'),
//         nextSection = thisSection.next(),
//         openingSection = $(href);

//     // hide right sections



//     if (href !== '#') {
//         allLiSection.removeClass('selected');
//         thisLi.addClass('selected');
//         nextSection.not(openingSection).velocity('animation.slideToRight', { display: 'inline-block', zIndex: '=-1' });
        

//         //openingSection.velocity('slideRight');


//         /*var nextNav = el.parents('nav').stop().next(),
//             ul = nextNav.find('.' + el.data('menu-link'));

//         ul.addClass('menu-open');*/
//     } else {
//         //openingSection.velocity('slideLeft');
//         //allSections.velocity({width: 0});
//     }
// });