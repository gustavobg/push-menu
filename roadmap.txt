Menu toggle

Ações:

Botão de abrir menu: (a, .btn)
	- Abrir primeira section
	- Abrir sections já expandidas
	- (Anim) usar o efeito de cascata e slide to right.

Clicar fora do menu (document)
	- Fechar todo o menu
	- Salvar estado aberto
	
Item de menu (li > a)
	- Expandir section lateral
	- Recolher section lateral se aberta
	- Exibir estado ativo se aberta
	
Objetivos visuais:
	- Adicionar ícone
	- Ocultar ícone de seta caso não haja seção adjacente
	- Exibir contador de itens.
	
Objetivos de interação.
	- Integrar ao tema para usá-lo como migalha de pão

Interno
	- getDataFromHTML - método que extrai de marcação html informações para formar o JSON data
	
	